import 'dart:math';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';

void main() {
  return runApp(
    MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Color(0xffDEC3C3),
        appBar: GradientAppBar(
          elevation: 0.0,
          centerTitle: true,
          title: Text(
            'Acak Dadu',
            style: TextStyle(
                color: Colors.grey.shade500,
                fontWeight: FontWeight.bold,
                fontSize: 23,
                fontFamily: 'Ubuntu',
                letterSpacing: 2),
          ),
          gradient: LinearGradient(
            begin: Alignment.topRight,
            end: Alignment.bottomLeft,
            colors: [
              Color(0xffE8DFDB),
              Color(0xffF2F2F2),
            ],
          ),
        ),
        body: DaduPage(),
      ),
    ),
  );
}

class DaduPage extends StatefulWidget {
  @override
  _DaduPageState createState() => _DaduPageState();
}

class _DaduPageState extends State<DaduPage> {
  int gantiNomorKiri = 1;
  int gantiNomorKanan = 1;

  void gantiNomor() {
    setState(() {
      gantiNomorKiri = Random().nextInt(6) + 1;
      gantiNomorKanan = Random().nextInt(6) + 1;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Row(
        children: <Widget>[
          Expanded(
            child: FlatButton(
              onPressed: () {
                gantiNomor();
              },
              child: SvgPicture.asset(
                'assets/images/dadu$gantiNomorKiri.svg',
                color: Colors.blueGrey,
                semanticsLabel: 'A red up arrow',
              ),
            ),
          ),
          Expanded(
            child: FlatButton(
              onPressed: () {
                gantiNomor();
              },
              child: SvgPicture.asset(
                'assets/images/dadu$gantiNomorKanan.svg',
                color: Colors.blueGrey,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
